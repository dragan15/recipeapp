﻿namespace Recipes.viewmodel
{
    public class RecipeViewModel
    {

        public string Name { get; set; }
        public string Image { get; set; }
        public string MoreInfo { get; set; }
    }

}

