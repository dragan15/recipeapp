﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Recipes.viewmodel;
using Xamarin.Forms;

namespace Recipes
{
    public partial class RecipePage : ContentPage
    {
        public ObservableCollection<RecipeViewModel> Recipe { get; set; }
        public RecipePage()
        {
            InitializeComponent();


            Recipe = new ObservableCollection<RecipeViewModel>();
            Recipe.Add(new RecipeViewModel { Name = "Hamburger", MoreInfo = "Beef, cheese, picles, mayo, buns" , Image = "Hamburger.png"});
            Recipe.Add(new RecipeViewModel { Name = "DoubleBaconBurger", MoreInfo = "Beef,bacon,cheese,more bacon,bun", Image = "DoubleBaconBurger.png" });
            Recipe.Add(new RecipeViewModel { Name = "ChickenParmesian", Image = "chicken.png", MoreInfo = "chicken, eggs, breadcrumbs, parmesian" });
            Recipe.Add(new RecipeViewModel { Name = "ChickenSalad", MoreInfo = "chicken, salad", Image = "salad.png" });
            Recipe.Add(new RecipeViewModel { Name = "SalmonwithButter", Image = "Salmon.png", MoreInfo = "salmon, garlic, butter" });
            lstView.ItemsSource = Recipe;
        }
  

    }
}
